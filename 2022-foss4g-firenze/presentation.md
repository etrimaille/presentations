---
marp: true
title:  Dataviz in QGIS and on the web

paginate: true
theme: gaia
class: invert
header: '![height:30px](../media/faunalia_name.svg) ![height:30px](../media/qgis-logo.png) ![height:30px](../media/logo-3liz.png)'
footer: '![height:30px](media/foss4g-logo.png) FOSS4G - Florence 2022'
size: 4:3
style: |
  section {
    font-size: 1.6em;
  }
  section.lead {
    background: #51a351;
  }
  section.invert {
    background: #51a351;
  }
  h1 {
    font-size: 1.4em;
  }
  ul li {
    font-size: 0.9em;
  }
headingDivider: 1
---

#  Dataviz in QGIS and on the web


<!-- _class: lead gaia-->
<br/>

# QGIS + Plotly = DataPlotly
<!-- _class: lead gaia-->

![qgis-plotly](media/qgisplotly.png)

# Framework
<!-- _class: lead gaia-->

![dataplotly-framework](media/framework.png)

powered by: [QGIS](https://qgis.org/), [Plotly](https://plotly.com/) and [Qt](https://www.qt.io/)

# Easy as it seems
<!-- _class: lead gaia-->

![dataplotly-framework](media/dataplotly.gif)

# Available in Print Layout
<!-- _class: lead gaia-->

![static-layout](media/static.jpeg)

# Also Atlas based
<!-- _class: lead gaia-->

![dataplotly-framework](media/dataplotly-atlas.gif)

# Lizmap Web Client

<!-- _class: lead gaia-->

* **QGIS** web interface : display, layer editing with forms, attribute table...
* Opensource project hosted on [GitHub](https://github.com/3liz/lizmap-web-client)

![width:700px](media/lizmap-web-client-demo.png)

# Plotly in Lizmap

* Adding dataviz on layers
* [Earthquake demo on https://demo.lizmap.com](https://demo.lizmap.com/lizmap/index.php/view/map/?repository=feat1&project=time_manager_earthquake)

![dataviz-panel-lizmap](media/dataviz-lizmap.gif)


# Print from Lizmap

![bg fit](media/print-lizmap.jpg)

# Dataplotly plugin on QGIS Server

<!-- _class: lead gaia-->
* QGIS Server
  * To be able to print **layouts** made on desktop **but** from the web

<br>

* Add QGIS **plugins** on QGIS Server 🚀
  * Make QGIS Server aware about **plotly** in a QGIS layout

# Thanks

<!-- _class: lead gaia-->

Matteo Ghetta, Faunalia
@ghtmtt


Etienne Trimaille, 3Liz
@etrimaille
